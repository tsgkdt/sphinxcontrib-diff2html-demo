=================================
Side by Side View
=================================


.. diff2html::
  :drawtype: side-by-side

  diff --git a/package.json b/package.json
  index e118816..503761b 100644
  --- a/package.json
  +++ b/package.json
  @@ -57,7 +57,6 @@
       "hogan.js": "^3.0.2"
     },
     "devDependencies": {
  -    "autoprefixer": "^6.3.6",
       "browserify": "^13.0.0",
       "clean-css": "^3.4.10",
       "codacy-coverage": "^1.1.3",
  @@ -67,7 +66,6 @@
       "mkdirp": "^0.5.1",
       "mocha": "^2.4.5",
       "nopt": "^3.0.6",
  -    "postcss-cli": "^2.5.2",
       "uglifyjs": "^2.4.10"
     },
     "license": "MIT",
  diff --git a/scripts/release.sh b/scripts/release.sh
  index 82ae9e5..839cc16 100755
  --- a/scripts/release.sh
  +++ b/scripts/release.sh
  @@ -31,7 +31,7 @@ rm -rf ${OUTPUT_DIR}
   mkdir -p ${OUTPUT_DIR}

   echo "Minifying ${OUTPUT_CSS_FILE} to ${OUTPUT_MIN_CSS_FILE}"
  -postcss --use autoprefixer ${INPUT_CSS_FILE} -d ${OUTPUT_DIR}
  +cp -f ${INPUT_CSS_FILE} ${OUTPUT_CSS_FILE}
   cleancss --advanced --compatibility=ie8 -o ${OUTPUT_MIN_CSS_FILE} ${OUTPUT_CSS_FILE}

   echo "Pre-compile hogan.js templates"
  diff --git a/src/ui/css/diff2html.css b/src/ui/css/diff2html.css
  index 88f41a4..9d4096d 100644
  --- a/src/ui/css/diff2html.css
  +++ b/src/ui/css/diff2html.css
  @@ -16,8 +16,6 @@
   }

   .d2h-file-stats {
  -  display: -webkit-box;
  -  display: -ms-flexbox;
     display: flex;
     margin-left: auto;
     font-size: 14px;
  @@ -43,12 +41,8 @@
   }

   .d2h-file-name-wrapper {
  -  display: -webkit-box;
  -  display: -ms-flexbox;
     display: flex;
  -  -webkit-box-align: center;
  -      -ms-flex-align: center;
  -          align-items: center;
  +  align-items: center;
     width: 100%;
     font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
     font-size: 15px;
  @@ -248,8 +242,6 @@
   }

   .d2h-file-list-line {
  -  display: -webkit-box;
  -  display: -ms-flexbox;
     display: flex;
     text-align: left;
   }
  @@ -308,8 +300,6 @@
   }

   .d2h-tag {
  -  display: -webkit-box;
  -  display: -ms-flexbox;
     display: flex;
     font-size: 10px;
     margin-left: 5px;
  @@ -347,21 +337,12 @@
   .selecting-right td.d2h-code-side-linenumber * {
     -webkit-touch-callout: none;
     -webkit-user-select: none;
  +  -khtml-user-select: none;
     -moz-user-select: none;
     -ms-user-select: none;
     user-select: none;
   }

  -.selecting-left .d2h-code-line::-moz-selection,
  -.selecting-left .d2h-code-line *::-moz-selection
  -.selecting-right td.d2h-code-linenumber::-moz-selection,
  -.selecting-left .d2h-code-side-line::-moz-selection,
  -.selecting-left .d2h-code-side-line *::-moz-selection,
  -.selecting-right td.d2h-code-side-linenumber::-moz-selection,
  -.selecting-right td.d2h-code-side-linenumber *::-moz-selection {
  -  background: transparent;
  -}
  -
   .selecting-left .d2h-code-line::selection,
   .selecting-left .d2h-code-line *::selection
   .selecting-right td.d2h-code-linenumber::selection,

